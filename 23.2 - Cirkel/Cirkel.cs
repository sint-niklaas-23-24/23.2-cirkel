﻿namespace _23._2___Cirkel
{
    public class Cirkel
    {
        public Cirkel() { }
        //Attributen
        private double _straal;
        //constructors
        public Cirkel(double straal)
        {
            Straal = straal;
        }
        //Properties
        public double Straal
        {
            get { return _straal; }
            set { _straal = value; }
        }

        public double BerekenOmtrek()
        {
            return (Straal * 2) * Math.PI;
        }
        public double BerekenOppervlakte()
        {
            return Math.PI * Math.Pow(Straal, 2);
        }
        public string FormattedOmtrek()
        {
            return BerekenOmtrek().ToString("0.00");
        }
        public string FormattedOppervlakte()
        {
            return BerekenOppervlakte().ToString("0.00");

        }
        public override string ToString()
        {
            return "Omtrek:".PadRight(15) + FormattedOmtrek() + Environment.NewLine + "Oppervlakte:".PadRight(15) + FormattedOppervlakte();
        }
    }
}
