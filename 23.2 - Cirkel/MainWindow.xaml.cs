﻿using System.Windows;

namespace _23._2___Cirkel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }


        private void BtnBerekenen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int straal = Convert.ToInt32(TxtStraal.Text);
                Cirkel cirkelEen = new Cirkel(straal);
                LblOmtrek.Content = cirkelEen.FormattedOmtrek();
                LblOppervlakte.Content = cirkelEen.FormattedOppervlakte();
                lblOverzicht.Content = cirkelEen.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("U gaf geen numerieke waarde in!", "Foute invoer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}