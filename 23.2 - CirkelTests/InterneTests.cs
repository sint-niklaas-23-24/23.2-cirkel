﻿namespace _23._2___CirkelTests
{
    public class InterneTests
    {
        [Test]
        public void CirkelOmtreKOppervlakte()
        {

            //Arrange (gegevens)
            int a = 3;
            //Act (uitvoeren)
            double omtrek = Math.Round((a * 2) * Math.PI, 2);
            double oppervlakte = Math.Round(Math.PI * Math.Pow(a, 2), 2);
            //Assert (resultaat)
            Assert.IsTrue(omtrek == 18.85 && oppervlakte == 28.27);
        }
    }
}
